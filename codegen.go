package restful

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
)

// CodeGenerator can create code generation middlewares
type CodeGenerator interface {
	Clients(next http.Handler) http.Handler
	Servers(next http.Handler) http.Handler
}

// NewSwaggerCodeGenerator creates an instance of CodeGenerator able to create code generation middlewares
func NewSwaggerCodeGenerator(swaggerGenURL, apiURL url.URL) CodeGenerator {
	apiURL.Path = "/swagger.json"

	return swggen{
		genURL:  swaggerGenURL,
		specURL: apiURL.String(),
	}
}

type swggen struct {
	genURL  url.URL
	specURL string
}

// Creates a middleware to generate swagger-clients code on /clients/{language}
func (cg swggen) Clients(next http.Handler) http.Handler {
	return codegenRedirect(
		regexp.MustCompile("/clients/([a-z0-9-]+)"),
		newClientApplicant(cg.genURL, cg.specURL),
		next,
	)
}

// Creates a middleware to generate swagger-servers code on /servers/{framework}
func (cg swggen) Servers(next http.Handler) http.Handler {
	return codegenRedirect(
		regexp.MustCompile("/servers/([a-z0-9-]+)"),
		newServerApplicant(cg.genURL, cg.specURL),
		next,
	)
}

func codegenRedirect(
	urlPattern *regexp.Regexp,
	reqc RequestCreator,
	next http.Handler,
) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		if m := urlPattern.MatchString(r.URL.Path); m {
			lang := urlPattern.FindStringSubmatch(r.URL.Path)[1]

			if link, err := getLink(reqc(lang)); nil != err {
				http.Error(rw, fmt.Sprint(err), http.StatusInternalServerError)
			} else {
				http.Redirect(rw, r, link, http.StatusFound)
			}

			return
		}

		next.ServeHTTP(rw, r)
	})
}

func getLink(req *http.Request) (string, error) {
	f := struct {
		Link string `json:"link"`
	}{}

	if rsp, err := http.DefaultClient.Do(req); nil != err {
		return "", errors.New("generate request error")
	} else if bb, err := ioutil.ReadAll(rsp.Body); nil != err {
		return "", errors.New("could not read generation result")
	} else if err := json.Unmarshal(bb, &f); nil != err {
		return "", errors.New("could not parse generation result")
	} else if 0 == len(f.Link) {
		return "", errors.New("generation failed")
	}

	return f.Link, nil
}

// RequestCreator creates http request due to the desired lanauge/framework
type RequestCreator func(option string) *http.Request

func newClientApplicant(genURL url.URL, specURL string) RequestCreator {
	var body *bytes.Reader
	if bb, err := json.Marshal(struct {
		SwaggerURL string `json:"swaggerUrl"`
	}{
		SwaggerURL: specURL,
	}); nil != err {
		panic(fmt.Sprintf("could not marshal request body: %s", err))
	} else {
		body = bytes.NewReader(bb)
	}

	return func(lang string) *http.Request {
		genURL.Path = fmt.Sprintf("/api/gen/clients/%s", lang)
		requrl := genURL.String()

		body.Seek(0, 0)
		req, _ := http.NewRequest(http.MethodPost, requrl, body)
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Accept", "application/json")

		return req
	}
}

func newServerApplicant(genURL url.URL, specURL string) RequestCreator {
	var body io.Reader
	if bb, err := json.Marshal(struct {
		SwaggerURL string `json:"swaggerUrl"`
	}{
		SwaggerURL: specURL,
	}); nil != err {
		panic(fmt.Sprintf("could not marshal request body: %s", err))
	} else {
		body = bytes.NewReader(bb)
	}

	return func(lang string) *http.Request {
		genURL.Path = fmt.Sprintf("/api/gen/servers/%s", lang)
		requrl := genURL.String()

		req, _ := http.NewRequest(http.MethodPost, requrl, body)
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Accept", "application/json")

		return req
	}
}
