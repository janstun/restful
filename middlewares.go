package restful

import (
	"net/http"
	"net/url"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/runtime/middleware"
	"github.com/rs/cors"
)

// Spec creates a middleware to serve api Spec on /swagger.json
func Spec(swgSpec *loads.Document) middleware.Builder {
	return func(next http.Handler) http.Handler {
		return middleware.Spec(swgSpec.BasePath(), swgSpec.Raw(), next)
	}
}

// CrossOriginResourceSharing enables CORS
func CrossOriginResourceSharing(debug bool, origins, headers, methods []string) middleware.Builder {
	h := cors.New(cors.Options{
		Debug:          debug,
		AllowedOrigins: origins,
		AllowedHeaders: headers,
		AllowedMethods: methods,
	})

	return h.Handler
}

// Redoc creates a middleware to serve api documentation on /docs
func Redoc(swgSpec *loads.Document) middleware.Builder {
	var title string
	spec := swgSpec.Spec()
	if nil != spec && nil != spec.Info && "" != spec.Info.Title {
		title = spec.Info.Title
	}

	opts := middleware.RedocOpts{
		BasePath: swgSpec.BasePath(),
		Title:    title,
	}

	return func(next http.Handler) http.Handler {
		return middleware.Redoc(opts, next)
	}
}

// UserInterface creates a middleware to redirect / to swagger-UserInterface on
func UserInterface(swgUIURL, apiURL url.URL) middleware.Builder {
	q := swgUIURL.Query()
	apiURL.Path = "/swagger.json"
	q.Set("url", apiURL.String())
	swgUIURL.RawQuery = q.Encode()
	redirection := swgUIURL.String()

	return Redirect("/", redirection)
}

// Redirect creates a middleware to Redirect path to Redirect
func Redirect(path, redirect string) middleware.Builder {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			if r.URL.Path == path {
				http.Redirect(rw, r, redirect, http.StatusMovedPermanently)

				return
			}

			next.ServeHTTP(rw, r)
		})
	}
}
