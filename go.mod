module gitlab.com/janstun/restful

go 1.15

require (
	github.com/go-openapi/loads v0.19.5
	github.com/go-openapi/runtime v0.19.22
	github.com/rs/cors v1.7.0
)
